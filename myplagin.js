/**
 * Created by pasko on 11.05.17.
 */
jQuery(document).ready(function($) {

    $('#mysubmit').click(function(){

        var data = {
            action: 'my_action',  // тоже самое, что в wp_ajax_my_action хуке
            title: $('#mytitle').val(),
            from_date: $('#myfrom_date').val()
        };

        jQuery.post(ajaxurl, data, function(response) {
            $("div#myplagindiv").empty();
            $('div#myplagindiv').html(response);
        });
    });
});
