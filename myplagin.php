<?php
/*
Plugin Name: My Plagin
Description: Show posts what you want.
Version: 1.0
Author: Pasko Dmitry
*/
?>
<?php
/*  Copyright 2017  Pasko_dmitry  (email: PaskoDima@meta.ua)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
?>
<?php
define( 'MY_PLAGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'MY_PLAGIN__URL', plugin_dir_url( __FILE__ ) );

class MyPlaginWidget extends WP_Widget {
	public function __construct() {
		parent::__construct( "my_plagin_widget", "My Plagin Widget",
			array( "description" => "A simple widget to show how WP Plugins work" ) );
	}

	public function form( $instance ) {
		$post_count = "";
		// если instance не пустой, достанем значения
		if ( ! empty( $instance ) ) {
			$post_count = $instance["post_count"];
		}

		$tableId   = $this->get_field_id( "post_count" );
		$tableName = $this->get_field_name( "post_count" );
		echo '<label for="' . $tableId . '">Post count</label><br>';
		echo '<input id="' . $tableId . '" type="text" name="' .
		     $tableName . '" value="' . $post_count . '"><br>';
	}

	public function update( $newInstance, $oldInstance ) {
		$values               = array();
		$values["post_count"] = htmlentities( $newInstance["post_count"] );

		return $values;
	}

	public function widget( $args, $instance ) {

		$post_count = $instance["post_count"];

		$args = array(
			'post_type'      => 'post',
			'posts_per_page' => "$post_count"
		);

		$query = new WP_Query( $args );

		echo '<label for="mytitle">Title</label><br>';
		echo '<input id="mytitle" type="search" name="mytitle" value=""><br>';
		echo '<label for="myfrom_date">From Date</label><br>';
		echo '<input id="myfrom_date" type="date" name="myfrom_date" value=""><br>';
		echo '<input id="mysubmit" type="submit" name="enter" value="Enter"><br><br>';

		echo '<div id="myplagindiv">';
		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				?><a href="<?php the_permalink() ?>"
                     title="Link : <?php the_title_attribute(); ?>"><?php the_title(); ?></a><?php
				echo '<br><br>';
			}
		}
		echo '</div>';
	}
}


add_action( "widgets_init", function () {
	register_widget( "MyPlaginWidget" );
} );


add_action( 'wp_ajax_my_action', 'my_action_callback' );
add_action( 'wp_ajax_nopriv_my_action', 'my_action_callback' );

//Подключаю js файл плагина
function wptuts_scripts_basic() {
	wp_register_script( 'myplagin-script', plugins_url( '/myplagin.js', __FILE__ ) );
	wp_enqueue_script( 'myplagin-script' );
}

add_action( 'wp_enqueue_scripts', 'wptuts_scripts_basic' );


add_action( 'wp_enqueue_scripts', 'myajax_data', 99 );
function myajax_data() {

	wp_localize_script( 'jquery', 'ajaxurl',
		array(
			'url' => admin_url( 'admin-ajax.php' )
		) );
}


function my_action_callback() {

	$mytitle     = $_POST['title'];
	$myfrom_date = $_POST['from_date'];

	$args  = array(
		'post_type'  => 'post',
		's'          => $mytitle,
		'date_query' => array(
			array(
				'after' => $myfrom_date
			)
		)
	);
	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			?><a href="<?php the_permalink() ?>"
                 title="Link : <?php the_title_attribute(); ?>"><?php the_title(); ?></a><br><br><?php
		}
	}
	wp_die();// выход нужен для того, чтобы в ответе не было ничего лишнего, только то что возвращает функция
}
